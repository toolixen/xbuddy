package dctimx.xbuddy

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import dctimx.xbuddy.kotlinx.inflate
import dctimx.xbuddy.kotlinx.message
import kotlinx.android.synthetic.main.row_category.view.*

/**
 * Created by Jose on 23/02/2018.
 */
class CategoriesAdapter(val categories:List<Category>, val activity: CategoriesActivity):
        RecyclerView.Adapter<CategoriesAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =  holder.bind(categories[position],activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.row_category))

    override fun getItemCount() = categories.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(category: Category, activity: CategoriesActivity) = with(itemView){

            image.setImageResource(category.resId)
            name.text = category.name

            item.setOnClickListener {

                activity.clickCategory(category)

            }

        }

    }
}