package dctimx.xbuddy.search_prefessionals

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.Settings
import android.support.multidex.MultiDex
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.View
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.places.Places
import com.google.android.gms.location.places.Places.GeoDataApi
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import dctimx.xbuddy.CategoriesActivity
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.hide
import dctimx.xbuddy.kotlinx.init
import dctimx.xbuddy.kotlinx.show
import kotlinx.android.synthetic.main.activity_map.*

class MapActivity : AppCompatActivity(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private var mMarker: Marker? = null
    private lateinit var mGoogleApiClient: GoogleApiClient
    private lateinit var selectLatLn: LatLng

    // Constants
    private val UPDATE_INTERVAL: Long = 5 * 1000
    private val FASTEST_INTERVAL: Long = 2 * 1000
    private val PERMISSON_LOCATION_CODE_REQUEST = 1007

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_map)

        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)

        rvProfessionals.init(ProfessionalsHorizontalMapAdapter(this), LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false))

        rv_categories_to_filter.init(TagToFilterAdapter(this), LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false))
        rv_professional_to_filter.init(TagToFilterAdapter(this), LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false))

        back.setOnClickListener { onBackPressed() }

        action_open_layout_filter.setOnClickListener {
            if(layout_filter.visibility == View.VISIBLE){
                layout_filter.hide()
            } else {
                layout_filter.show()
            }
        }

        layout_filter.setOnClickListener { layout_filter.hide() }

        action_apply_filter.setOnClickListener { layout_filter.hide() }

        toggle.setOnCheckedChangeListener { _ , i ->

            if(i == R.id.off){

                off.text = "On"
                on.text = "Off"
                rvProfessionals.setBackgroundColor(ContextCompat.getColor(this,R.color.colorBackgroundActivity))
                rvProfessionals.layoutParams.height = RecyclerView.LayoutParams.MATCH_PARENT
                rvProfessionals.requestLayout()
                rvProfessionals.init(ProfessionalsVerticalMapAdapter(this), LinearLayoutManager(this, LinearLayoutManager.VERTICAL,false))

            }
            if(i == R.id.on){

                off.text = "Off"
                on.text = "On"
                rvProfessionals.setBackgroundColor(ContextCompat.getColor(this,android.R.color.transparent))
                rvProfessionals.layoutParams.height = RecyclerView.LayoutParams.WRAP_CONTENT
                rvProfessionals.requestLayout()
                rvProfessionals.init(ProfessionalsHorizontalMapAdapter(this), LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,false))

            }
        }

        MultiDex.install(this)

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(object: GoogleApiClient.ConnectionCallbacks {
                    override fun onConnected(p0: Bundle?) {
                        checkLocationSelected()
                    }
                    override fun onConnectionSuspended(p0: Int) {
                        mGoogleApiClient.connect()
                    }
                })
                .addOnConnectionFailedListener({
                    connectionResult ->
                    //TODO En caso de fallo en conexion a servicios de Google.
                    Log.i("GoogleApiConnection", "Connection failed. Error: " + connectionResult.errorCode)
                })
                .enableAutoManage(this, {
                    connectionResult ->
                    //TODO En caso de fallo en conexion a servicios de Google.
                    Log.i("GoogleApiConnection", "Connection failed AutoManage. Error: " + connectionResult.errorCode)
                })
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build()

        fabSelectPosition.setOnClickListener {
            if (fabSelectPosition.visibility == View.VISIBLE) {
                imgMarker.visibility = View.GONE
                fabSelectPosition.visibility = View.GONE
                addMarker(selectLatLn)
            }
        }

        checkLocation()
    }

    private fun checkLocationSelected() {
        val location = CategoriesActivity.instance.location
        if ( location!!.id == "actual" || location!!.id == "selecciona" ){
            Log.i("Point", "Entre por aqui")
            initLocationServices()
        } else {
            Log.i("Point", "Entre por acaa ->" + location!!.title)
            GeoDataApi.getPlaceById(mGoogleApiClient, location.id).setResultCallback { unit ->
                val pos = Location(LocationManager.GPS_PROVIDER)
                unit.forEach { place ->
                    pos.latitude = place.latLng.latitude
                    pos.longitude = place.latLng.longitude
                }
                updateMapPosition(pos)
            }
        }
    }

    override fun onStart() {
        super.onStart()
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect()
        }
    }

    override fun onStop() {
        super.onStop()
        if (mGoogleApiClient.isConnected) {
            mGoogleApiClient.disconnect()
        }
    }


    private fun checkLocation(): Boolean {
        val locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        val isLocationEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)

        if (!isLocationEnabled) {
            AlertDialog.Builder(this)
                    .setTitle(resources.getString(R.string.alert_enable_location_title))
                    .setMessage(resources.getString(R.string.alert_enable_location_msg))
                    .setPositiveButton(resources.getString(R.string.alert_enable_location_btn_positive), { _ , _ ->
                        val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                        startActivity(myIntent)
                    })
                    .setNegativeButton(resources.getString(R.string.alert_enable_location_btn_cancel), { _ , _ ->
                        //FIXME que realizar si el usuario no tiene activada su ubicacion
                        finish()
                    })
                    .show()
        }

        return isLocationEnabled
    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
    }

    private val cameraIdleListener = GoogleMap.OnCameraIdleListener { selectLatLn = mMap.cameraPosition.target }

    fun updateMapPosition(mLocation: Location) {
        val pos = LatLng(mLocation.latitude, mLocation.longitude)
        selectLatLn = pos

        //FIXME cambiar el titulo de la ubicacion
        //FIXME en caso de no requerir puntero con la ubicacion quitarlo o personalizarlo
        if (CategoriesActivity.instance.location!!.id == "selecciona") {
            imgMarker.visibility = View.VISIBLE
            fabSelectPosition.visibility = View.VISIBLE
            mMap.setOnCameraIdleListener(cameraIdleListener)
        } else {
            addMarker(pos)
        }

        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(pos, 15.0f))
    }

    fun addMarker(pos: LatLng) {
        if ( mMarker == null)
            mMarker = mMap.addMarker(MarkerOptions().position(pos).title("Su ubicacion"))
        else
            mMarker!!.position = pos
    }

    @SuppressLint("MissingPermission") //Se realiza previa validacion de permisos de ubicacion
    fun initLocationServices() {
        if ( !hasLocationPermission() ) return

        //NOTE desactivada la funcion de actualizar ubicacio
        //startLocationUpdates()
        Log.i("Point", "Entre por aca")

        var fusedLocationProviderClient : FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)
        fusedLocationProviderClient.lastLocation.addOnSuccessListener(this, { location ->
            if (location != null) {
                Log.i("Point", "Entre por upin")
                updateMapPosition(location)
            }
            else
                Log.i("Point", "Entre por acuya")
        })
    }

    @SuppressLint("MissingPermission") //se ah validado antes el permiso de ubicacion
    protected fun startLocationUpdates() {
        val mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL)

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, {
            location -> updateMapPosition(location)
        })
    }

    private fun hasLocationPermission(): Boolean {
        val permissons = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)

        if (ActivityCompat.checkSelfPermission(this, permissons[0]) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, permissons[1]) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissons, PERMISSON_LOCATION_CODE_REQUEST)
            return false
        }
        return true
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if ( requestCode == PERMISSON_LOCATION_CODE_REQUEST ) {
            if ( grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED )
                initLocationServices()
            else
                AlertDialog.Builder(this)
                        .setTitle(resources.getString(R.string.alert_grant_permission_title))
                        .setMessage(resources.getString(R.string.alert_grant_permission_msg))
                        .setPositiveButton(resources.getString(R.string.alert_grant_permission_btn_positive), { _ , _ ->
                            initLocationServices()
                        })
                        .setNegativeButton(resources.getString(R.string.alert_grant_permission_btn_cancel), { _ , _ ->
                            //FIXME que realizar si el usuario no da permiso para acceder a la ubicacion
                            finish()
                        })
                        .show()
        }
    }

}
