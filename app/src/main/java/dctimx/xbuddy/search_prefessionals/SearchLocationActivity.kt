package dctimx.xbuddy.search_prefessionals

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.location.places.AutocompleteFilter
import dctimx.xbuddy.Location
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.init
import kotlinx.android.synthetic.main.activity_search_location.*
import com.google.android.gms.location.places.Places
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.LatLngBounds
import dctimx.xbuddy.CategoriesActivity
import java.util.concurrent.TimeUnit


class SearchLocationActivity : AppCompatActivity() {

    private val context = this
    private lateinit var mGoogleApiClient: GoogleApiClient
    private var loadMore:Boolean = true
    var searchLocationAdapter: SearchLocationAdapter? = null
    var listLocationResult: ArrayList<Location>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_location)

        back.setOnClickListener {
            finish()
        }

        resetLocationList()

        lstLocation.addItemDecoration(DividerItemDecoration(
            lstLocation.context,
            LinearLayoutManager.VERTICAL
        ))

        txtSearchLocation.addTextChangedListener(object: TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                if ( s.toString().length > 3 )
                    getPlaces(s.toString())
                else {
                    loadMore = false
                    resetLocationList()
                }
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })

        mGoogleApiClient = GoogleApiClient.Builder(this)
                .addConnectionCallbacks(object: GoogleApiClient.ConnectionCallbacks {
                    override fun onConnected(p0: Bundle?) {}
                    override fun onConnectionSuspended(p0: Int) {
                        mGoogleApiClient.connect()
                    }
                })
                .addOnConnectionFailedListener({
                    connectionResult ->
                    //TODO En caso de fallo en conexion a servicios de Google.
                    Log.i("GoogleApiConnection", "Connection failed. Error: " + connectionResult.errorCode)
                })
                .enableAutoManage(this, {
                    connectionResult ->
                    //TODO En caso de fallo en conexion a servicios de Google.
                    Log.i("GoogleApiConnection", "Connection failed AutoManage. Error: " + connectionResult.errorCode)
                })
                .addApi(Places.PLACE_DETECTION_API)
                .addApi(Places.GEO_DATA_API)
                .build()

    }

    private fun resetLocationList() {
        listLocationResult = ArrayList<Location>().apply {
            add(Location("actual", "Ubicación actual",""))
            add(Location("selecciona", "Seleccionar ubicación en mapa",""))
        }
        searchLocationAdapter = SearchLocationAdapter(listLocationResult!!, this)
        searchLocationAdapter!!.setOnClickListener(onClickListener)
        lstLocation.init(searchLocationAdapter!!)
    }

    private fun getPlaces(place: String) {
        val southWest = LatLng(-85.0, -180.0)
        val northEast = LatLng(85.0, 180.0)
        val bounds = LatLngBounds(southWest, northEast)

        Places.GeoDataApi.getAutocompletePredictions(mGoogleApiClient, place, bounds, AutocompleteFilter.Builder().setTypeFilter(AutocompleteFilter.TYPE_FILTER_NONE).build())
                .setResultCallback({ autocompletePredictions ->
                    if( autocompletePredictions.status.isSuccess) {
                        val iterator = autocompletePredictions.iterator()
                        val listLocations = arrayListOf<Location>()
                        while (iterator.hasNext()) {
                            val prediction = iterator.next()
                            listLocations.add(Location(prediction.placeId, prediction.getPrimaryText(null).toString(), prediction.getSecondaryText(null).toString()))
                        }
                        listLocationResult = listLocations
                        searchLocationAdapter = SearchLocationAdapter(listLocations, context)
                        searchLocationAdapter!!.setOnClickListener(onClickListener)
                        lstLocation.init(searchLocationAdapter!!)
                    } else {
                        Log.e("autocompletePredictions", "not success: " + autocompletePredictions.status)
                    }
                    autocompletePredictions.release()
                },60, TimeUnit.SECONDS)
    }

    private var onClickListener: View.OnClickListener = View.OnClickListener { v ->
        val ind =  lstLocation.getChildAdapterPosition(v)
        CategoriesActivity.instance.location = listLocationResult!![ind]
        finish()
    }
}
