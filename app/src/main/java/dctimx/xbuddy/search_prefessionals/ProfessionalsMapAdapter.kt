package dctimx.xbuddy.search_prefessionals

import android.support.v7.widget.RecyclerView
import android.view.View
import dctimx.xbuddy.Professional
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.launchActivity
import dctimx.xbuddy.schedulin.PersonalProfileActivity
import dctimx.xbuddy.schedulin.SchedulingActivity
import kotlinx.android.synthetic.main.row_professional_map_vertical_class.view.*

/**
 * Created by Gildardo Ibarra on 17/04/18.
 */
abstract class ProfessionalsMapAdapter(open val activity: MapActivity): RecyclerView.Adapter<ProfessionalsMapAdapter.ViewHolder>() {

    //FIXME este arreglo se puede recibir como parametro cuando se implemente datos reales.
    private val profiles = arrayOf(
            Professional("Francisco Alarcon",R.drawable.profile_img, "Electricista"),
            Professional("Ana Chavez",R.drawable.profile_img_min_2, "Muchacha"),
            Professional("Pablo Dominguez",R.drawable.profile_img, "Electricista"),
            Professional("Belen Lopez",R.drawable.profile_img_min_2, "Muchacha"),
            Professional("Claudio Ibarra", R.drawable.profile_img, "Fontanero"),
            Professional("Elizabeth Dominguez", R.drawable.profile_img_min_2, "Niñera"),
            Professional("Angel Gallardo", R.drawable.profile_img, "Cocinero"),
            Professional("Maria Lucia", R.drawable.profile_img_min_2, "Niñera"),
            Professional("Santiago Morales", R.drawable.profile_img, "Pintor"),
            Professional("Jose Marin", R.drawable.profile_img, "Albañil"),
            Professional("Ulises Moreno", R.drawable.profile_img, "Albañil"),
            Professional("Karla Rios", R.drawable.profile_img_min_2, "Albañil")
    )


    override fun onBindViewHolder(holder: ProfessionalsMapAdapter.ViewHolder, position: Int) = holder.bind(activity, position, profiles)

    override fun getItemCount() = 12

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(activity: MapActivity, position: Int, profiles: Array<Professional>) = with(itemView){

            action_profile.setOnClickListener {
                activity.launchActivity<PersonalProfileActivity>()
            }

            action_scheduler.setOnClickListener {
                activity.launchActivity<SchedulingActivity>()
            }

            lblName.text = profiles[position].name
            lblCategory.text = profiles[position].category
            image.setImageResource(profiles[position].image)
        }

    }
}