package dctimx.xbuddy.search_prefessionals

import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.inflate
import kotlinx.android.synthetic.main.row_tag_filter.view.*

/**
 * Created by Jose on 16/3/2018.
 */
class TagToFilterAdapter(val activity: MapActivity): RecyclerView.Adapter<TagToFilterAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.row_tag_filter))

    override fun getItemCount() = 12

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var focud = false

        fun bind(activity: MapActivity) = with(itemView){

            item.setOnClickListener {

                if(focud){
                    focud = false
                    item.setBackgroundResource(R.drawable.backgrond_row_filter_stroke)
                    item.setTextColor(ContextCompat.getColor(context,R.color.colorWhite))
                } else {
                    focud = true
                    item.setBackgroundResource(R.drawable.backgrond_row_filter)
                    item.setTextColor(ContextCompat.getColor(context,R.color.colorAccent))

                }

            }


        }

    }
}