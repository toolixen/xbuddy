package dctimx.xbuddy.search_prefessionals

import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import com.google.android.gms.location.places.Places.GeoDataApi
import dctimx.xbuddy.*
import dctimx.xbuddy.kotlinx.inflate
import kotlinx.android.synthetic.main.row_search_location.view.*

/**
 * Created by anzendigital on 19/04/18.
 */
class SearchLocationAdapter(val locations:List<Location>, val activity: AppCompatActivity):
        RecyclerView.Adapter<SearchLocationAdapter.ViewHolder>(), View.OnClickListener {

    private var listener: View.OnClickListener? = null

    override fun onBindViewHolder(holder: SearchLocationAdapter.ViewHolder, position: Int) {
        holder.bind(locations[position],activity)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView: View = parent.inflate(R.layout.row_search_location)
        itemView.setOnClickListener(this)
        return ViewHolder(itemView)
    }

    override fun getItemCount() = locations.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(location: Location, activity: AppCompatActivity) = with(itemView){
            locationTitle.text = location.title
            locationSubTitle.text = location.subTitle
        }
    }

    fun setOnClickListener(listener: View.OnClickListener) {
        this.listener = listener
    }

    override fun onClick(v: View?) {
        if (listener != null)
            listener!!.onClick(v)
    }

}