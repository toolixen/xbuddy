package dctimx.xbuddy.search_prefessionals

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import dctimx.xbuddy.Professional
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.inflate
import dctimx.xbuddy.kotlinx.launchActivity
import dctimx.xbuddy.schedulin.PersonalProfileActivity
import dctimx.xbuddy.schedulin.SchedulingActivity
import kotlinx.android.synthetic.main.row_professional_map_vertical_class.view.*

/**
 * Created by Jose on 16/3/2018.
 */
class ProfessionalsVerticalMapAdapter(override val activity: MapActivity): ProfessionalsMapAdapter(activity) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfessionalsMapAdapter.ViewHolder = ViewHolder(parent.inflate(R.layout.row_professional_map_vertical_class))

}