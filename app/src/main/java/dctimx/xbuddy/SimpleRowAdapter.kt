package dctimx.xbuddy

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import dctimx.xbuddy.kotlinx.inflate
import dctimx.xbuddy.kotlinx.init
import kotlinx.android.synthetic.main.row_simple_string.view.*

/**
 * Created by Jose on 23/02/2018.
 */
class SimpleRowAdapter(val strings:List<String>, val activity: CategoriesActivity):
        RecyclerView.Adapter<SimpleRowAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =  holder.bind(strings[position],activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.row_simple_string))

    override fun getItemCount() = strings.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(category: String, activity: CategoriesActivity) = with(itemView){

            name.text = category

        }

    }
}