package dctimx.xbuddy

import android.annotation.SuppressLint
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.view.View
import dctimx.xbuddy.auth.LanguagesAdapter
import dctimx.xbuddy.kotlinx.anim
import dctimx.xbuddy.kotlinx.init
import dctimx.xbuddy.kotlinx.launchActivity
import dctimx.xbuddy.search_prefessionals.MapActivity
import dctimx.xbuddy.search_prefessionals.SearchLocationActivity
import kotlinx.android.synthetic.main.activity_categories.*
import android.content.Intent
import android.provider.Settings
import android.support.v7.app.AlertDialog


class CategoriesActivity : AppCompatActivity() {

    companion object {
        lateinit var instance: CategoriesActivity
    }

    var location: Location? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)

        instance = this

        val professionalCategory = ArrayList<Category>().apply {
            add(Category("Profesionales", R.drawable.ic_profesionales))
            add(Category("Casa", R.drawable.ic_casa))
            add(Category("Autos", R.drawable.ic_auto))
            add(Category("Cocina", R.drawable.ic_cocina))
            add(Category("Mascotas", R.drawable.ic_mascotas))
            add(Category("Tecnologia", R.drawable.ic_tecnologia))
            add(Category("Otros", R.drawable.ic_otros))
            add(Category("Buscar", R.drawable.ic_busqueda))
        }

        rvCategories.init(
                CategoriesAdapter(professionalCategory, this),
                GridLayoutManager(this,4)
        )

        dropdowm_languages.getList().init(LanguagesAdapter(ArrayList<Language>().apply {

            add(Language("Aleman"))
            add(Language("Albano"))
            add(Language("Arabe"))
            add(Language("Armenio"))
            add(Language("Belaruso"))
            add(Language("Bengales"))
            add(Language("Español"))
            add(Language("Frances"))
            add(Language("Ingles"))
            add(Language("Portugues"))

        },this))

        rvAll.init(AllAdapter(ArrayList<CategoryHome>().apply {
            add(CategoryHome("Electricista",R.drawable.ic_electricista,"15",null))
            add(CategoryHome("Fontaneros",R.drawable.ic_fontaneros,"7",null))
            add(CategoryHome("Evanistas",R.drawable.ic_evanistas,"35",null))
            add(CategoryHome("Muchachas",R.drawable.ic_muchachas,"10",null))
            add(CategoryHome("Limpiaza pesada",R.drawable.ic_limpieza_pesada,"25",null))
            add(CategoryHome("Cocineros",R.drawable.ic_cocinero,"4",null))
            add(CategoryHome("Niñeras",R.drawable.ic_ninera,"20",null))
            add(CategoryHome("Mantenimiento residencial",R.drawable.ic_mantenimiento_residencial,"7",ArrayList<String>().apply {
                add("Colgar cuadros")
                add("Colocar cortinas")
                add("Cambiar filtros")
                add("Cambiar lamparas")
                add("Montar un mueble")
                add("Instalar una persiana")
                add("Soporte de television microondas")
                add("Instalar mirilla")
                add("Cambiar manguera del gas")
            }))
            add(CategoryHome("Pintor",R.drawable.ic_pintor,"28",null))
            add(CategoryHome("Tecnico de electrodomesticos",R.drawable.ic_mantenimiento_electrodomesticos,"10",ArrayList<String>().apply {
                add("Item #1")
                add("Item #2")
                add("Item #3")
                add("Item #4")
            }))
            add(CategoryHome("Albañil",R.drawable.ic_albanil,"13",null))
        }, this))

        close_search.setOnClickListener {
            wrapper_search.anim(android.R.anim.fade_out)
            wrapper_search.visibility = View.GONE
            rvCategories.visibility = View.VISIBLE
        }

        txtSearchLocation.setOnClickListener {
            launchActivity<SearchLocationActivity>()
        }

    }

    @SuppressLint("SetTextI18n")
    override fun onResume() {
        super.onResume()
        if ( location != null) {
            txtSearchLocation.setText(location!!.title)
        }
    }

    fun clickCategory(category: Category) {

        when(category.name){
            "Buscar" -> {
                wrapper_search.anim(android.R.anim.fade_in)
                wrapper_search.visibility = View.VISIBLE
                rvCategories.visibility = View.INVISIBLE
            }
            else -> {
                if(wrapper_search.visibility == View.VISIBLE){
                    wrapper_search.anim(android.R.anim.fade_out)
                    wrapper_search.visibility = View.GONE
                    rvCategories.visibility = View.VISIBLE
                }
                if ( location != null)
                    launchActivity<MapActivity>()
                else
                    launchActivity<SearchLocationActivity>()
            }
        }

    }
}
