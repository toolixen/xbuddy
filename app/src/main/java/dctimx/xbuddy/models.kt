package dctimx.xbuddy

/**
 * Created by Jose on 22/02/2018.
 */

data class Language(val name:String)

data class Category(val name:String, val resId:Int)

data class CategoryHome(val name:String, val resId:Int, val num:String, val content:List<String>?)

class Message(val image:Int, val message:String, val hour:String, val type:Int)

class ReviewComment(val image:Int,val name:String,val rate:String,val title:String,val commentary:String)

data class ResponseDefault (
    val status:Boolean,
    val msg:String
)

data class Professional(val name:String, val image:Int, val category:String)

data class Location(val id:String?, val title:String, val subTitle:String, val latitude: String? = null, val longitud:String? = null)