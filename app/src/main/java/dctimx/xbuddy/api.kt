package dctimx.xbuddy

import com.pixplicity.easyprefs.library.Prefs
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.OkHttpClient
import okhttp3.RequestBody
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

/**
 * Created by Jose on 19/3/2018.
 */
interface api {

    @FormUrlEncoded
    @POST("Cliente/Login")
    fun signIn(@Field("email") email:String, @Field("password") password:String): Observable<ResponseDefault>

    @FormUrlEncoded
    @POST("Cliente/AddCliente")
    fun signUp(@Field("name") name:String,
               @Field("last_name") last_name:String,
               @Field("email") email:String,
               @Field("phone") phone:String,
               @Field("password") password:String
    ): Observable<ResponseDefault>

    @FormUrlEncoded
    @POST("Cliente/AddCliente")
    fun rememberPassword(@Field("old_password") old_password:String,
                         @Field("new_password") new_password:String
    ): Observable<ResponseDefault>

    @FormUrlEncoded
    @POST("Cliente/AddPreferencias")
    fun addPreferences(@Field("sexo") sexo:String,
                       @Field("estatura") estatura:String,
                       @Field("peso") peso:String,
                       @Field("fecha_n") fecha_nacimiento:String,
                       @Field("nivel") nivel:String,
                       @Field("ejercicios[]") ejercicios:ArrayList<String>
    ): Observable<ResponseDefault>

    @FormUrlEncoded
    @POST("Cliente/AddMembresia")
    fun AddMembresia(@Field("tarifa") tarifa:String, @Field("token_card") token_card:String): Observable<ResponseDefault>

    @POST("Cliente/GetMembresia")
    fun getCurrentPlan(): Observable<ResponseDefault>

    @POST("Cliente/GetTarifas")
    fun getPlans(): Observable<ResponseDefault>

    @POST("Cliente/GetClases")
    fun getClasess(): Observable<ResponseDefault>

    @POST("Cliente/GetMisClases")
    fun getMyClasess(): Observable<ResponseDefault>

    @FormUrlEncoded
    @POST("Cliente/InscribirClases")
    fun joinToClass(@Field("clase") clase:String, @Field("dia") dia:String): Observable<ResponseDefault>

    companion object {

        fun create(timeout:Long = 1): api {

            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl("http://ws.dctimx.com/Gym/index.php/")
                    .client( OkHttpClient.Builder()
                            .readTimeout(timeout, TimeUnit.MINUTES)
                            .connectTimeout(timeout, TimeUnit.MINUTES)
                            .addInterceptor { interceptor ->

                                val original = interceptor.request()
                                val originalHttpUrl = original.url()

                                val url = originalHttpUrl.newBuilder()
                                        .addQueryParameter("token", Prefs.getString(TOKEN,"TOKEN_EMPTY") )
                                        .build()

                                // Request customization: add request headers
                                val requestBuilder = original.newBuilder()
                                        /*.addHeader("X-AYLIEN-NewsAPI-Application-ID", "91bc7207")
                                        .addHeader("X-AYLIEN-NewsAPI-Application-Key", "7b7208dbad3a322e5dada75a9c26f05b")*/
                                        .url(url)

                                val request = requestBuilder.build()
                                interceptor.proceed(request)

                            }.build() )
                    .build()

            return retrofit.create(api::class.java)

        }

    }

}