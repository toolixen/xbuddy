package dctimx.xbuddy.schedulin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import dctimx.xbuddy.R
import dctimx.xbuddy.chat.ChatActivity
import dctimx.xbuddy.kotlinx.launchActivity
import kotlinx.android.synthetic.main.activity_scheduling.*
import net.cachapa.expandablelayout.ExpandableLayout

class SchedulingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scheduling)

        action_scheduler.setOnClickListener {
            launchActivity<ChatActivity>()
        }

        back.setOnClickListener {
            onBackPressed()
        }

        action_day.setOnClickListener {
            onClickExpandableLayout(expandable_layout_day)
        }

        action_hour.setOnClickListener {
            onClickExpandableLayout(expandable_layout_hour)
        }

        action_description.setOnClickListener {
            onClickExpandableLayout(expandable_layout_description)
        }

        action_payment.setOnClickListener {
            onClickExpandableLayout(expandable_layout_payment)
        }

    }

    private fun onClickExpandableLayout(expandableLayout: ExpandableLayout) {
        if(expandableLayout.isExpanded) {
            expandableLayout.collapse()
        } else {
            expandableLayout.expand()
        }
    }
}
