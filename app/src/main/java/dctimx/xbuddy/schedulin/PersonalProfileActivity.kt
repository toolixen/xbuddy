package dctimx.xbuddy.schedulin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import dctimx.xbuddy.ProfessionalReviewCommentsAdapter
import dctimx.xbuddy.R
import dctimx.xbuddy.ReviewComment
import dctimx.xbuddy.kotlinx.init
import dctimx.xbuddy.kotlinx.launchActivity
import kotlinx.android.synthetic.main.activity_personal_profile.*

class PersonalProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_personal_profile)

        val reviewComments = ArrayList<ReviewComment>().apply {
            add(ReviewComment(R.drawable.profile_img_min, "Juan Gabriel", "3.0", "Excelente", getString(R.string.lorem_ipsum)))
            add(ReviewComment(R.drawable.profile_img_min_2, "Mellisa Olaya", "3.0", "Gran trabajo", getString(R.string.hipsum)))
            add(ReviewComment(R.drawable.profile_img_min, "Angelica Rodriguez", "3.0", "EL mejor", getString(R.string.lorem_ipsum)))
            add(ReviewComment(R.drawable.profile_img_min_2, "Maria Martinez", "3.0", "Increible", getString(R.string.hipsum)))
            add(ReviewComment(R.drawable.profile_img_min, "Michelle Jackson", "3.0", "Fantastico", getString(R.string.lorem_ipsum)))
            add(ReviewComment(R.drawable.profile_img_min_2, "Adriana Garcia", "3.0", "Excelente", getString(R.string.hipsum)))
            add(ReviewComment(R.drawable.profile_img_min, "Steven Palacios", "3.0", "Recomendable", getString(R.string.lorem_ipsum)))
        }

        rvComments.init(ProfessionalReviewCommentsAdapter(reviewComments))

        action_schedule_appoinment.setOnClickListener {
            launchActivity<SchedulingActivity>()
        }

        back.setOnClickListener {
            onBackPressed()
        }

        nScrollView.post( object: Runnable {
            override fun run() {
                nScrollView.scrollTo(0,0);
            }
        });

    }
}
