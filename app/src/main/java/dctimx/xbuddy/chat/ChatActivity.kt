package dctimx.xbuddy.chat

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.inputmethod.EditorInfo
import dctimx.xbuddy.Message
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.init
import kotlinx.android.synthetic.main.activity_chat.*

class ChatActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_chat)
        setSupportActionBar(toolbar)

        val adapter = RequestServiceChatAdapter(activity = this)
        rvChat.init(adapter)

        adapter.addMessage(
                Message(
                        image = R.drawable.profile_img_min,
                        message = "Holaaa",
                        hour = "11:32",
                        type = RequestServiceChatAdapter.ITEM_TYPE_EMISOR_FIRST
                )
        )

        adapter.addMessage(
                Message(
                        image = R.drawable.profile_img_min_2,
                        message = "Que tal?",
                        hour = "11:33",
                        type = RequestServiceChatAdapter.ITEM_TYPE_RECEIVER_FIRST
                )
        )

        adapter.addMessage(
                Message(
                        image = R.drawable.profile_img_min,
                        message = "Todo excelente",
                        hour = "11:33",
                        type = RequestServiceChatAdapter.ITEM_TYPE_EMISOR_FIRST
                )
        )

        adapter.addMessage(
                Message(
                        image = R.drawable.profile_img_min,
                        message = "Y tu como estas?",
                        hour = "11:34",
                        type = RequestServiceChatAdapter.ITEM_TYPE_EMISOR_SINGLE
                )
        )

        adapter.addMessage(
                Message(
                        image = R.drawable.profile_img_min_2,
                        message = "Me alegra :-)",
                        hour = "11:35",
                        type = RequestServiceChatAdapter.ITEM_TYPE_RECEIVER_FIRST
                )
        )

        adapter.addMessage(
                Message(
                        image = R.drawable.profile_img_min_2,
                        message = "De maravilla",
                        hour = "11:35",
                        type = RequestServiceChatAdapter.ITEM_TYPE_RECEIVER_SINGLE
                )
        )

        adapter.addMessage(
                Message(
                        image = R.drawable.profile_img_min_2,
                        message = getString(R.string.lorem_ipsum),
                        hour = "11:37",
                        type = RequestServiceChatAdapter.ITEM_TYPE_RECEIVER_SINGLE
                )
        )

        var first = true

        inputMessage.setOnFocusChangeListener { view, b ->
            if(b){
                scroll.post {
                    //sroll.smoothScrollTo(0, sroll.bottom + sroll.paddingBottom + actionVoice.height)
                    scroll.smoothScrollTo(0, scroll.bottom)
                }
            } else {

            }
        }

        actionVoice.setOnClickListener {

        }

        inputMessage.setOnEditorActionListener { textView, actionId, keyEvent ->
            if(actionId == EditorInfo.IME_ACTION_DONE){

                adapter.addMessage(
                        Message(
                                image = R.drawable.profile_img_min,
                                message = inputMessage.text.toString(),
                                hour = "11:37",
                                type = if (first) RequestServiceChatAdapter.ITEM_TYPE_EMISOR_FIRST else RequestServiceChatAdapter.ITEM_TYPE_EMISOR_SINGLE
                        )
                )

                scroll.post {
                    //sroll.smoothScrollTo(0, sroll.bottom + sroll.paddingBottom + actionVoice.height)
                    scroll.smoothScrollTo(0, scroll.bottom)
                }

                inputMessage.setText("")
                first = false
            }
            return@setOnEditorActionListener true
        }

        back.setOnClickListener {
            onBackPressed()
        }
    }
}
