package dctimx.xbuddy.chat

import android.animation.ValueAnimator
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.LinearInterpolator
import dctimx.xbuddy.Message
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.anim
import dctimx.xbuddy.kotlinx.density
import dctimx.xbuddy.kotlinx.inflate
import kotlinx.android.synthetic.main.row_message_chat_emisor_first.view.*

/**
 * Created by Jose on 26/02/2018.
 */
open class RequestServiceChatAdapter(
        var messages:MutableList<Message> = ArrayList(),
        val activity: ChatActivity):RecyclerView.Adapter<RequestServiceChatAdapter.ViewHolder>() {

    companion object {

        val ITEM_TYPE_EMISOR_FIRST = 0
        val ITEM_TYPE_EMISOR_SINGLE = 1
        val ITEM_TYPE_RECEIVER_FIRST = 2
        val ITEM_TYPE_RECEIVER_SINGLE = 3

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =  holder.bind(messages[position],activity)

    override fun getItemViewType(position: Int) = messages[position].type

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int):ViewHolder {

        return when(viewType){
            ITEM_TYPE_EMISOR_FIRST -> ViewHolder(parent.inflate(R.layout.row_message_chat_emisor_first))
            ITEM_TYPE_EMISOR_SINGLE -> ViewHolder(parent.inflate(R.layout.row_message_chat_emisor_single))
            ITEM_TYPE_RECEIVER_FIRST -> ViewHolder(parent.inflate(R.layout.row_message_chat_receiver_first))
            ITEM_TYPE_RECEIVER_SINGLE -> ViewHolder(parent.inflate(R.layout.row_message_chat_receiver_single))
            else -> ViewHolder(parent.inflate(R.layout.row_message_chat_emisor_first))
        }

    }

    override fun getItemCount() = messages.size

    fun addMessage(msg: Message){
        messages.add(msg)
        notifyItemInserted(messages.size - 1)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(msg: Message, activity: ChatActivity) = with(itemView){

            if(itemViewType == ITEM_TYPE_EMISOR_FIRST || itemViewType == ITEM_TYPE_RECEIVER_FIRST){
                image.setImageResource(msg.image)
            }

            message.text = msg.message
            hour.text = msg.hour

            wrapperMessage.setOnClickListener {

                when(hour.visibility){
                    View.GONE -> {

                        val animator = ValueAnimator.ofInt(wrapperHour.height, 24 * wrapperHour.density)
                        animator.addUpdateListener { valueAnimator ->
                            wrapperHour.layoutParams.height = valueAnimator.animatedValue as Int
                            wrapperHour.requestLayout();
                        }
                        animator.duration = 200
                        animator.interpolator = LinearInterpolator()
                        animator.start()

                        Handler().postDelayed({
                            hour.anim(R.anim.show_hour_message)
                            hour.visibility = View.VISIBLE
                        },100)

                    }
                    View.VISIBLE -> {

                        hour.anim(R.anim.hide_hour_message)
                        hour.visibility = View.GONE

                        Handler().postDelayed({

                            val animator = ValueAnimator.ofInt(wrapperHour.height, 4 * wrapperHour.density)
                            animator.addUpdateListener { valueAnimator ->
                                wrapperHour.layoutParams.height = valueAnimator.animatedValue as Int
                                wrapperHour.requestLayout();
                            }
                            animator.duration = 200
                            animator.interpolator = AccelerateDecelerateInterpolator()
                            animator.start()

                        },100)


                    }
                }

            }

        }

    }
}