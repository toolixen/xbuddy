package dctimx.xbuddy

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import dctimx.xbuddy.kotlinx.inflate
import dctimx.xbuddy.kotlinx.init
import kotlinx.android.synthetic.main.row_all.view.*

/**
 * Created by Jose on 23/02/2018.
 */
class AllAdapter(val categories:List<CategoryHome>, val activity: CategoriesActivity):
RecyclerView.Adapter<AllAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =  holder.bind(categories[position],activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.row_all))

    override fun getItemCount() = categories.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(category: CategoryHome, activity: CategoriesActivity) = with(itemView){

            if(category.content != null){
                expandable.expandable = true
                expandable.setIcon(true)
                expandable.getList().init(SimpleRowAdapter(category.content,activity))
            }

            expandable.setText(category.name)
            expandable.setNum(category.num);
            expandable.setImageDrawable(category.resId)

        }

    }
}