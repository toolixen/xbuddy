package dctimx.xbuddy.auth

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dctimx.xbuddy.CategoriesActivity
import dctimx.xbuddy.R
import dctimx.xbuddy.country.CountriesActivity
import dctimx.xbuddy.kotlinx.launchActivity
import kotlinx.android.synthetic.main.activity_payment_methods.*

class PaymentMethodsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_payment_methods)

        action_country.setOnClickListener {
            startActivityForResult(Intent(this, CountriesActivity::class.java),CountriesActivity.COUNTRY_REQUEST)
        }

        action_send.setOnClickListener {
            launchActivity<CategoriesActivity>()
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == CountriesActivity.COUNTRY_REQUEST){

            if(resultCode == CountriesActivity.COUNTRY_SELECTED) {

                val name = data!!.getStringExtra(CountriesActivity.COUNTRY_NAME)
                val image = data.getIntExtra(CountriesActivity.COUNTRY_IMAGE,0)

                image_country.setImageResource(image)
                text_country.text = name

            }

        }

    }
}
