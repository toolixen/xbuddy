package dctimx.xbuddy.auth

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import dctimx.xbuddy.api
import dctimx.xbuddy.CategoriesActivity
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.InputValidate
import dctimx.xbuddy.kotlinx.launchActivity
import dctimx.xbuddy.kotlinx.showMaterialDialog
import dctimx.xbuddy.kotlinx.validateInputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_sign_in.*
import kotlinx.android.synthetic.main.fragment_sign_in.view.*

/**
 * Created by Jose on 21/02/2018.
 */
class SignInFragment : Fragment() {

    val apiService by lazy { api.create() }
    lateinit var dispose: Disposable

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_in, container, false)
    }

    override fun onStart() {
        super.onStart()

        view!!.action_remember_password.setOnClickListener {
            activity!!.launchActivity<RememberPasswordActivity>()
        }

        view!!.action_login.setOnClickListener { signIn() }

    }

    private fun signIn() {
        val inputs = ArrayList<InputValidate>()

        inputs.add(InputValidate(phone.getEditText(),"El telefono es obligatorio."))
        if(!Patterns.PHONE.matcher(phone.getText()).matches()){
            activity!!.showMaterialDialog("El telefono no es correcto.", "Oops", {dialog ->
                phone.getEditText().requestFocus()
            })
            return
        }
        inputs.add(InputValidate(password.getEditText(),"La contraseña es obligatoria."))

        if(!activity!!.validateInputs(inputs)) return

        val progressDialog = MaterialDialog.Builder(activity!!)
            .content("Ingresando")
            .progress(true, 0)
            .show()

        dispose = apiService.signIn(phone.getText().toString(), password.getText().toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->

                    progressDialog.dismiss()

                    if(result.status) {

                        //Prefs.putString(Pref.TOKEN,result.token)

                        startActivity(
                            Intent(activity, CategoriesActivity::class.java).apply {
                                addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                            }
                        )

                    } else {

                        activity!!.showMaterialDialog("El usuario o la contraseña es incorrecto")

                    }

                }, { error ->

                    progressDialog.dismiss()
                    activity!!.showMaterialDialog("Ha ocurrido un error.\n\n${error.message}")

                }
            )

    }
}