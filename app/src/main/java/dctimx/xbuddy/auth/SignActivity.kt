package dctimx.xbuddy.auth

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.widget.LinearLayout
import android.widget.TextView
import dctimx.xbuddy.R
import dctimx.xbuddy.country.CountriesActivity
import dctimx.xbuddy.kotlinx.*
import kotlinx.android.synthetic.main.activity_personal_profile.*
import kotlinx.android.synthetic.main.activity_sign.*


class SignActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sign)

        tabLayout.addTab(tabLayout.newTab().setText("Iniciar Sesión"))
        tabLayout.addTab(tabLayout.newTab().setText("Registrate"))

        val tabLinearLayout = tabLayout.getChildAt(0) as LinearLayout

        val tv1 = (tabLinearLayout.getChildAt(0) as LinearLayout).getChildAt(1) as TextView
        tv1.scaleY = -1f
        val tv2 = (tabLinearLayout.getChildAt(1) as LinearLayout).getChildAt(1) as TextView
        tv2.scaleY = -1f

        var fragment: Fragment = SignInFragment()
        supportFragmentManager.beginTransaction().replace(R.id.fragment, fragment).commit()

        tabLayout.addOnTabSelectedListener(object : OnTabChangeListener(){
            override fun onTabSelected(tab: TabLayout.Tab?) {

                when (tab!!.position){
                    0 -> fragment = SignInFragment()
                    1 -> fragment = SignUpFragment()
                }

                supportFragmentManager.beginTransaction().replace(R.id.fragment, fragment).commit();
            }

        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        //Country request result
        if(requestCode == CountriesActivity.COUNTRY_REQUEST && resultCode == CountriesActivity.COUNTRY_SELECTED) {
            supportFragmentManager.findFragmentById(R.id.fragment).onActivityResult(requestCode, resultCode, data)
        }

    }

}
