package dctimx.xbuddy.auth

import android.app.Activity
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import dctimx.xbuddy.Language
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.inflate
import kotlinx.android.synthetic.main.row_language.view.*

/**
 * Created by Jose on 22/02/2018.
 */
class LanguagesAdapter(val languages:List<Language>, val activity: Activity):
        RecyclerView.Adapter<LanguagesAdapter.ViewHolder>() {

    private val languagesSelected = ArrayList<Language>()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =  holder.bind(languages[position],activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.row_language))

    override fun getItemCount() = languages.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(language: Language, activity: Activity) = with(itemView){

            name.text = language.name

            item.setOnClickListener {

                checkbox.isChecked = !checkbox.isChecked

            }

        }

    }
}