package dctimx.xbuddy.auth

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dctimx.xbuddy.Language
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.init
import dctimx.xbuddy.kotlinx.launchActivity
import kotlinx.android.synthetic.main.activity_choose_languages.*

class ChooseLanguagesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_choose_languages)

        rvLanguages.init(LanguagesAdapter(ArrayList<Language>().apply {

            add(Language("Aleman"))
            add(Language("Albano"))
            add(Language("Arabe"))
            add(Language("Armenio"))
            add(Language("Belaruso"))
            add(Language("Bengales"))
            add(Language("Español"))
            add(Language("Frances"))
            add(Language("Ingles"))
            add(Language("Portugues"))

        },this))

        action_next.setOnClickListener {

            launchActivity<PaymentMethodsActivity>()

        }
    }
}
