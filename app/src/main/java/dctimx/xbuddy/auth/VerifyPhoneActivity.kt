package dctimx.xbuddy.auth

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatTextView
import android.widget.LinearLayout
import android.widget.Toast
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.OnTextChange
import dctimx.xbuddy.kotlinx.launchActivity
import kotlinx.android.synthetic.main.activity_verify_phone.*

class VerifyPhoneActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verify_phone)

        action_validate.setOnClickListener {
            launchActivity<ChooseLanguagesActivity>()
        }

        input_code.addTextChangedListener(object : OnTextChange() {
            override fun onTextChanged(char: CharSequence?, p1: Int, p2: Int, p3: Int) {

                val input = ( ( numbers.getChildAt(char!!.length -1) as LinearLayout ).getChildAt(0) as AppCompatTextView )
                Toast.makeText(this@VerifyPhoneActivity,char.length.toString(),Toast.LENGTH_SHORT).show()
                when(char.length){
                    1 -> {
                        input.text = input_code.text.subSequence(0,1)
                        ( ( numbers.getChildAt(1) as LinearLayout ).getChildAt(0) as AppCompatTextView ).text = ""
                    }
                    2 -> {
                        input.text = input_code.text.subSequence(1,2)
                        ( ( numbers.getChildAt(2) as LinearLayout ).getChildAt(0) as AppCompatTextView ).text = ""
                    }
                    3 -> {
                        input.text = input_code.text.subSequence(2,3)
                        ( ( numbers.getChildAt(3) as LinearLayout ).getChildAt(0) as AppCompatTextView ).text = ""
                    }
                    4 -> {
                        input.text = input_code.text.subSequence(3,4)
                    }
                }

            }
        })
    }
}
