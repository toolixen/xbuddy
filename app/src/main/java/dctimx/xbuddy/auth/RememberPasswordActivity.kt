package dctimx.xbuddy.auth

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.MenuItem
import dctimx.xbuddy.R
import kotlinx.android.synthetic.main.activity_remember_password.*

class RememberPasswordActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_remember_password)

        setSupportActionBar(toolbar)
        with(supportActionBar!!) {
            setHomeButtonEnabled(true)
            setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

}
