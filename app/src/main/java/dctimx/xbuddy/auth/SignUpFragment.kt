package dctimx.xbuddy.auth

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.afollestad.materialdialogs.MaterialDialog
import dctimx.xbuddy.api
import dctimx.xbuddy.R
import dctimx.xbuddy.country.CountriesActivity
import dctimx.xbuddy.kotlinx.InputValidate
import dctimx.xbuddy.kotlinx.launchActivity
import dctimx.xbuddy.kotlinx.showMaterialDialog
import dctimx.xbuddy.kotlinx.validateInputs
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_sign_up.view.*

/**
 * Created by Jose on 21/02/2018.
 */
class SignUpFragment : Fragment() {

    val apiService by lazy { api.create() }
    lateinit var dispose: Disposable

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_sign_up, container, false)
    }

    override fun onStart() {
        super.onStart()

        view!!.action_signup.setOnClickListener {
            activity!!.launchActivity<VerifyPhoneActivity>()
            singUp()
        }

        view!!.action_country.setOnClickListener {
            activity!!.startActivityForResult(Intent(activity, CountriesActivity::class.java),CountriesActivity.COUNTRY_REQUEST)
        }

    }

    private fun singUp() {
        val inputs = ArrayList<InputValidate>()

        inputs.add(InputValidate(view!!.name.getEditText(),"El nombre es obligatorio."))
        inputs.add(InputValidate(view!!.last_name.getEditText(),"El apellido es obligatorio."))

        if(view!!.sex.sexSelected.isEmpty()){
            activity!!.showMaterialDialog("El genero es obligatorio.", "Oops", {dialog ->
                view!!.phone.getEditText().requestFocus()
            })
            return
        }

        inputs.add(InputValidate(view!!.phone.getEditText(),"El telefono es obligatorio."))
        if(!Patterns.PHONE.matcher(view!!.phone.getText()).matches()){
            activity!!.showMaterialDialog("El telefono no es correcto.", "Oops", {dialog ->
                view!!.phone.getEditText().requestFocus()
            })
            return
        }

        inputs.add(InputValidate(view!!.password.getEditText(),"La contraseña es obligatoria."))
        inputs.add(InputValidate(view!!.confirm_password.getEditText(),"La confirmacion de la contraseña es obligatoria."))
        if(!view!!.password.getEditText().text.equals(view!!.confirm_password.getEditText().text)){
            activity!!.showMaterialDialog("Las contraseñas no coinciden.", "Oops", {dialog ->
                view!!.phone.getEditText().requestFocus()
            })
            return
        }

        if(!activity!!.validateInputs(inputs)) return

        val progressDialog = MaterialDialog.Builder(activity!!)
                .content("Registrando")
                .progress(true, 0)
                .show()

        dispose = apiService.signIn(view!!.phone.getText().toString(), view!!.password.getText().toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->

                    progressDialog.dismiss()

                    if(result.status) {

                        //Prefs.putString(Pref.TOKEN,result.token)

                        startActivity(
                                Intent(activity, VerifyPhoneActivity::class.java).apply {
                                    addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
                                }
                        )

                    } else {

                        activity!!.showMaterialDialog("El usuario o la contraseña es incorrecto")

                    }

                }, { error ->

                    progressDialog.dismiss()
                    activity!!.showMaterialDialog("Ha ocurrido un error.\n\n${error.message}")

                }
            )

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == CountriesActivity.COUNTRY_REQUEST && resultCode == CountriesActivity.COUNTRY_SELECTED) {

            val name = data!!.getStringExtra(CountriesActivity.COUNTRY_NAME)
            val image = data.getIntExtra(CountriesActivity.COUNTRY_IMAGE,0)

            view!!.image_country.setImageResource(image)
            view!!.text_country.text = name

        }

    }

}