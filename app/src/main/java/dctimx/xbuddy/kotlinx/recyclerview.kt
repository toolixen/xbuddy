package dctimx.xbuddy.kotlinx

import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * Created by Jose on 11/02/2018.
 */

/*Fun for set properties to recycler view */
fun RecyclerView.init(adapter:RecyclerView.Adapter<*>, layoutManager: RecyclerView.LayoutManager = LinearLayoutManager(context)) {

    this.isNestedScrollingEnabled = false
    this.layoutManager = layoutManager
    this.adapter = adapter

}

/*
* Extension for adapters of recyclers view
* */
fun ViewGroup.inflate(layoutRes: Int): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, false)
}
