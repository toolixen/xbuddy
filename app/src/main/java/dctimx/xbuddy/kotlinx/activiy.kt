package dctimx.xbuddy.kotlinx

import android.app.Activity
import android.content.DialogInterface
import android.content.Intent
import android.net.Uri
import android.os.Build
import android.provider.MediaStore
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.AppCompatEditText
import android.widget.EditText
import android.widget.Toast
import com.afollestad.materialdialogs.MaterialDialog

/**
 * Created by Jose on 09/02/2018.
 */

/* Start activity */
inline fun <reified T:Activity> Activity.launchActivity() {
    startActivity(Intent(this,T::class.java))
}

/* Message */
fun Activity.message(message:String){

    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

        Snackbar.make(window.decorView.rootView, message, Snackbar.LENGTH_LONG).show()

    } else {

        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()

    }

}

/* Valdiate Fields */

class InputValidate(val input: EditText, val message:String)

fun Activity.validateInputs(inputs : ArrayList<InputValidate>):Boolean{

    inputs.forEach {
        if (it.input.text.isEmpty()) {

            showMaterialDialog(it.message, "Oops", {dialog ->
                it.input.requestFocus()
            })

            return false

        }
    }

    return true

}

fun Activity.getPathURI(uri: Uri): String {
    val cursor = this.contentResolver.query(uri, null, null, null, null)
    if (cursor == null) {
        return uri.path
    } else {
        cursor.moveToFirst()
        val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
        return cursor.getString(idx)
    }
}

/*
* Extension show showMaterialDialog messages
* */
fun Activity.showMaterialDialog(message:String, title:String = "Oops", dismissListener:(DialogInterface) -> Unit = {}) {
    MaterialDialog.Builder(this)
        .title(title)
        .content(message)
        .positiveText("OK")
        .dismissListener(DialogInterface.OnDismissListener {
            dismissListener(it)
        })
        .show()
}