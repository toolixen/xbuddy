package dctimx.xbuddy.kotlinx

import android.graphics.drawable.LayerDrawable
import android.widget.ProgressBar
import dctimx.xbuddy.R

/**
 * Created by Jose on 13/02/2018.
 */

fun ProgressBar.init(progress:Int){

    val colors = context.resources.getIntArray(R.array.ratingBarReviews)

    this.progress = progress
    (progressDrawable as LayerDrawable)
        .getDrawable(2)
        .setColorFilter(colors[( progress * 64 ) / 100], android.graphics.PorterDuff.Mode.SRC_IN)

}