package dctimx.xbuddy.kotlinx

import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils

/**
 * Created by Jose on 09/02/2018.
 */


val View.density:Int
    get() = context.resources.displayMetrics.density.toInt()

private val View.marginLayoutParams: ViewGroup.MarginLayoutParams
    get() = layoutParams as ViewGroup.MarginLayoutParams

/*
* PADDING
*/
var View.padding:Int
    get() = 0
    set(value) {
        leftPadding = value
        topPadding = value
        rightPadding = value
        bottomPadding = value
    }

var View.leftPadding:Int
    get() {
        return paddingLeft
    }
    set(value) {
        setPadding((value * density),paddingTop,paddingRight,paddingBottom)
    }

var View.topPadding:Int
    get() {
        return paddingTop
    }
    set(value) {
        setPadding(paddingLeft,(value * density),paddingRight,paddingBottom)
    }

var View.rightPadding:Int
    get() {
        return paddingRight
    }
    set(value) {
        setPadding(paddingLeft,paddingTop,(value * density),paddingBottom)
    }

var View.bottomPadding:Int
    get() {
        return paddingBottom
    }
    set(value) {
        setPadding(paddingLeft,paddingTop,paddingRight,(value * density))
    }

/*
* MARGIN
*/

var View.margin:Int
    get() = 0
    set(value) {
        leftMargin = value
        topMargin = value
        rightMargin = value
        bottomMargin = value
    }

var View.leftMargin:Int
    get() {
        return marginLayoutParams.leftMargin
    }
    set(value) {
        marginLayoutParams.leftMargin = (value * density)
        requestLayout()
    }

var View.topMargin:Int
    get() {
        return marginLayoutParams.topMargin
    }
    set(value) {
        marginLayoutParams.topMargin = (value * density)
        requestLayout()
    }

var View.rightMargin:Int
    get() {
        return marginLayoutParams.rightMargin
    }
    set(value) {
        marginLayoutParams.rightMargin = (value * density)
        requestLayout()
    }

var View.bottomMargin:Int
    get() {
        return marginLayoutParams.bottomMargin
    }
    set(value) {
        marginLayoutParams.bottomMargin = (value * density)
        requestLayout()
    }

/*
* MARGIN
*/

fun View.anim(anim:Int) = this.startAnimation(AnimationUtils.loadAnimation(context, anim))

fun View.show(anim:Int = android.R.anim.fade_in){
    anim(anim)
    this.visibility = View.VISIBLE
}

fun View.hide(anim:Int = android.R.anim.fade_out){
    anim(anim)
    this.visibility = View.GONE
}