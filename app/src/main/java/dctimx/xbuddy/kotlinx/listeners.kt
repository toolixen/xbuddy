package dctimx.xbuddy.kotlinx

import android.support.design.widget.TabLayout
import android.support.v4.view.ViewPager
import android.text.Editable
import android.text.TextWatcher



/**
 * Created by Jose on 10/02/2018.
 */
abstract class OnPageChangeListener : ViewPager.OnPageChangeListener {

    override fun onPageScrollStateChanged(state: Int) {}

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) { }

}

abstract class OnTextChange : TextWatcher {

    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) { }

    override fun afterTextChanged(s: Editable) {}

}

abstract class OnTabChangeListener : TabLayout.OnTabSelectedListener {

    override fun onTabReselected(tab: TabLayout.Tab?) {}

    override fun onTabUnselected(tab: TabLayout.Tab?) {}

}