package dctimx.xbuddy.kotlinx

import android.net.Uri
import android.widget.ImageView
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso

/**
 * Created by Jose on 19/3/2018.
 */

fun ImageView.load(uri:Uri, success:() -> Unit = {}, error:() -> Unit = {}){
    Picasso.with(context).load(uri).into(this, object : Callback {
        override fun onSuccess() { success() }
        override fun onError() { error() }
    })
}