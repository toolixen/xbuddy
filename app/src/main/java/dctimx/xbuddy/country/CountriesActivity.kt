package dctimx.xbuddy.country

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.init
import dctimx.xbuddy.kotlinx.message
import kotlinx.android.synthetic.main.activity_countries.*

open class CountriesActivity : AppCompatActivity() {

    companion object {

        val COUNTRY_REQUEST: Int = 233
        val COUNTRY_SELECTED: Int = 234
        val COUNTRY_NAME: String = "COUNTRY_NAME"
        val COUNTRY_IMAGE: String = "COUNTRY_IMAGE"

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_countries)

        val adapter = CountryAdapter(ArrayList<Country>().apply {
            add(Country(R.drawable.mex, "Mexico"))
            add(Country(R.drawable.ury, "Uruguay"))
            add(Country(R.drawable.usa, "Estados Unidos"))
            add(Country(R.drawable.ven, "Venezuela"))
            add(Country(R.drawable.esp, "España"))
            add(Country(R.drawable.bra, "Brasil"))
            add(Country(R.drawable.arg, "Argentina"))
            add(Country(R.drawable.chl, "Chile"))
            add(Country(R.drawable.ecu, "Ecuador"))
            add(Country(R.drawable.col, "Colombia"))
            add(Country(R.drawable.cub, "Cuba"))
        },this)

        adapter.listener {
            message(it.name)

            setResult(COUNTRY_SELECTED, Intent().apply {
                putExtra(COUNTRY_NAME, it.name)
                putExtra(COUNTRY_IMAGE, it.resId)
            })

            finish()

        }

        rvCountries.init(adapter)

    }

}