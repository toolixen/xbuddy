package dctimx.xbuddy.country

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import dctimx.xbuddy.R
import dctimx.xbuddy.kotlinx.inflate
import kotlinx.android.synthetic.main.row_country.view.*

/**
 * Created by Jose on 19/02/2018.
 */
class CountryAdapter(val countries:List<Country>, val activity: CountriesActivity):
        RecyclerView.Adapter<CountryAdapter.ViewHolder>() {

    companion object { lateinit var callback: (Country) -> Unit }

    fun listener(listener:(Country) -> Unit ) {
        callback = listener
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =  holder.bind(countries[position],activity)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.row_country))

    override fun getItemCount() = countries.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(country: Country, activity: CountriesActivity) = with(itemView){

            name.text = country.name
            image.setImageResource(country.resId)

            item.setOnClickListener {

                callback(country)

            }

        }

    }
}