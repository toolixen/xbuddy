package dctimx.xbuddy

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import dctimx.xbuddy.kotlinx.inflate
import kotlinx.android.synthetic.main.row_professional_review_comment.view.*

/**
 * Created by Jose on 26/02/2018.
 */
class ProfessionalReviewCommentsAdapter(val reviewComments:List<ReviewComment>):
        RecyclerView.Adapter<ProfessionalReviewCommentsAdapter.ViewHolder>() {

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =  holder.bind(reviewComments[position])

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent.inflate(R.layout.row_professional_review_comment))

    override fun getItemCount() = reviewComments.size

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bind(reviewComment: ReviewComment) = with(itemView){

            image.setImageResource(reviewComment.image)

            //Picasso.with(context).load(reviewComment.image).into(image)

            name.text = reviewComment.name
            rate.text = reviewComment.rate
            title.text = reviewComment.title
            commentary.text = reviewComment.commentary

        }

    }

}