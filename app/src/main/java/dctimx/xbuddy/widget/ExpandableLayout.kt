package dctimx.xbuddy.widget

import android.content.Context
import android.os.Handler
import android.support.v7.widget.AppCompatTextView
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.LinearLayout
import dctimx.xbuddy.R
import kotlinx.android.synthetic.main.expandable_layout.view.*

/**
 * Created by Jose on 23/02/2018.
 */
class ExpandableLayout(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    var opened : Boolean = false
    var expandable : Boolean = false

    init {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {

        //Set properties to LinearLayout
        orientation = LinearLayout.VERTICAL

        //Inflate view
        inflate(context, R.layout.expandable_layout, this)

        wrapper.setOnClickListener {

            if(expandable == false) return@setOnClickListener

            if(opened){

                opened = false

                list.pivotY = 0f
                list.animate()
                        .scaleY(0f)
                        .setInterpolator(AccelerateDecelerateInterpolator())
                        .setDuration(100)

                Handler().postDelayed({
                    list.layoutParams.height = 0
                    list.requestLayout()
                }, 100)

            } else {

                opened = true

                list.layoutParams.height = WRAP_CONTENT
                list.requestLayout()

                list.pivotY = 0f
                list.animate()
                        .scaleY(1f)
                        .setInterpolator(AccelerateDecelerateInterpolator())
                        .setDuration(140)

            }

        }

    }

    fun getList() = list

    fun setText(string:String) {
        text.text = string
    }

    fun setNum(string:String) {
        num.text = string
    }

    fun setImageDrawable(resId:Int) {
        image.setImageResource(resId)
    }

    fun setIcon(boolean: Boolean) {

        if(boolean) {
            icon.visibility = View.VISIBLE
        }

    }

}