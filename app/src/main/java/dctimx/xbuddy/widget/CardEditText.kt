package dctimx.xbuddy.widget

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.LinearLayout
import dctimx.xbuddy.R
import kotlinx.android.synthetic.main.card_edit_text.view.*

/**
 * Created by Jose on 22/02/2018.
 */
class CardEditText(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    init {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {

        //Set properties to LinearLayout
        orientation = LinearLayout.VERTICAL

        //Inflate view
        inflate(context, R.layout.card_edit_text, this)

        text.setOnFocusChangeListener { view, b ->

            if(b){
                label.setTypeface(text.typeface, Typeface.BOLD)
                animateColorLine(colorFrom = R.color.colorGrayLight, colorTo = R.color.colorAccent)
                animateColorLabel(colorFrom = R.color.colorText, colorTo = R.color.colorAccent)

            } else {
                label.setTypeface(text.typeface, Typeface.NORMAL)
                animateColorLine(colorFrom = R.color.colorAccent, colorTo = R.color.colorGrayLight)
                animateColorLabel(colorFrom = R.color.colorAccent, colorTo = R.color.colorText)

            }
        }

    }

    private fun animateColorLine(colorFrom: Int, colorTo: Int) {
        val animator = ValueAnimator.ofObject(
                ArgbEvaluator(),
                ContextCompat.getColor(context, colorFrom),
                ContextCompat.getColor(context, colorTo)
        )
        animator.duration = 250
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener { line.setBackgroundColor(it.animatedValue as Int) }
        animator.start()
    }

    private fun animateColorLabel(colorFrom: Int, colorTo: Int) {
        val animator = ValueAnimator.ofObject(
                ArgbEvaluator(),
                ContextCompat.getColor(context, colorFrom),
                ContextCompat.getColor(context, colorTo)
        )
        animator.duration = 250
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener { label.setTextColor(it.animatedValue as Int) }
        animator.start()
    }

    fun getText() = text.text

}
