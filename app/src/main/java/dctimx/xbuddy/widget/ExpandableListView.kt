package dctimx.xbuddy.widget

import android.content.Context
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.View
import android.view.ViewGroup.LayoutParams.WRAP_CONTENT
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.LinearLayout
import com.mikepenz.iconics.Iconics.init
import dctimx.xbuddy.R
import kotlinx.android.synthetic.main.expandable_view.view.*

/**
 * Created by Jose on 23/02/2018.
 */
open class ExpandableListView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    var opened : Boolean = false

    init {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {

        //Set properties to LinearLayout
        orientation = LinearLayout.VERTICAL

        //Inflate view
        inflate(context, R.layout.expandable_view, this)

        //Apply atributes
        val attributes = context.theme.obtainStyledAttributes(attrs, R.styleable.ExpandableListView, 0, 0)

        try {

            text.text = attributes.getString(R.styleable.ExpandableListView_elv_text)

            val isLine = attributes.getBoolean(R.styleable.ExpandableListView_elv_line, true)
            val isArrow = attributes.getBoolean(R.styleable.ExpandableListView_elv_arrow, true)

            if(!isLine){
               line.visibility = View.GONE
            }

            if(!isArrow) {
                icon.visibility = View.GONE
            }


        } finally {
            attributes.recycle()
        }

        wrapper.setOnClickListener {

            if(opened){

                opened = false

                //list.pivotY = 0f
                list.animate()
                        .scaleY(0f)
                        .setInterpolator(AccelerateDecelerateInterpolator())
                        .setDuration(100)

                Handler().postDelayed({
                    list.layoutParams.height = 0
                    list.requestLayout()
                }, 100)



            } else {

                opened = true

                list.layoutParams.height = WRAP_CONTENT
                list.requestLayout()

                //list.pivotY = 0f
                list.animate()
                        .scaleY(1f)
                        .setInterpolator(AccelerateDecelerateInterpolator())
                        .setDuration(140)

            }

        }

    }

    fun getList() = list

}