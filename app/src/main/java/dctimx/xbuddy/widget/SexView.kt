package dctimx.xbuddy.widget

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.text.InputType
import android.util.AttributeSet
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.LinearLayout
import android.widget.RelativeLayout
import com.mikepenz.iconics.IconicsDrawable
import dctimx.xbuddy.R
import kotlinx.android.synthetic.main.widget_sex.view.*

/**
 * Created by Jose on 27/02/2018.
 */
open class SexView(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    private var callUnselect = false

    open var sexSelected:String = ""
        get(){
            return sexSelected
        }

    init {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {

        //Set properties to LinearLayout
        orientation = LinearLayout.VERTICAL

        //Inflate view
        inflate(context, R.layout.widget_sex, this)

        tab_male.setOnClickListener {

            sexSelected = "MALE"

            if(callUnselect) {

                if(tab_female.background != ContextCompat.getDrawable(context,R.color.colorGrayLight))
                    unselect(text_female,tab_female,icon_woman)

            }

            select(text_male,tab_male,icon_man)

            callUnselect = true

        }

        tab_female.setOnClickListener {

            sexSelected = "FEMALE"

            if(callUnselect) {

                if(tab_male.background != ContextCompat.getDrawable(context,R.color.colorGrayLight))
                    unselect(text_male,tab_male,icon_man)

            }

            select(text_female,tab_female,icon_woman)

            callUnselect = true

        }

    }

    private fun select(text: AppCompatTextView, tab: RelativeLayout, icon: AppCompatImageView) {
        animateBackgroundColor(R.color.colorGrayLight, R.color.colorAccent, tab)
        animateColorIcon(R.color.colorBlack, R.color.colorWhite, icon)
        animateColorText(R.color.colorIconDefault, R.color.colorWhite, text)
    }

    private fun unselect(text: AppCompatTextView, tab: RelativeLayout, icon: AppCompatImageView) {
        animateBackgroundColor(R.color.colorAccent, R.color.colorGrayLight, tab)
        animateColorIcon(R.color.colorWhite, R.color.colorBlack, icon)
        animateColorText(R.color.colorWhite, R.color.colorIconDefault, text)
    }

    private fun animateColorIcon(colorFrom: Int, colorTo: Int, icon: AppCompatImageView) {
        val animator = ValueAnimator.ofObject(
                ArgbEvaluator(),
                ContextCompat.getColor(context, colorFrom),
                ContextCompat.getColor(context, colorTo)
        )
        animator.duration = 250
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener { icon.setColorFilter(it.animatedValue as Int) }
        animator.start()
    }

    private fun animateBackgroundColor(colorFrom: Int, colorTo: Int, view: RelativeLayout) {
        val animator = ValueAnimator.ofObject(
                ArgbEvaluator(),
                ContextCompat.getColor(context, colorFrom),
                ContextCompat.getColor(context, colorTo)
        )
        animator.duration = 250
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener { view.setBackgroundColor(it.animatedValue as Int) }
        animator.start()
    }

    private fun animateColorText(colorFrom: Int, colorTo: Int, view: AppCompatTextView) {
        val animator = ValueAnimator.ofObject(
                ArgbEvaluator(),
                ContextCompat.getColor(context, colorFrom),
                ContextCompat.getColor(context, colorTo)
        )
        animator.duration = 250
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener { view.setTextColor(it.animatedValue as Int) }
        animator.start()
    }

}
