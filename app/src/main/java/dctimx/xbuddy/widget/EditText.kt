package dctimx.xbuddy.widget

import android.animation.ArgbEvaluator
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Typeface
import android.support.v4.content.ContextCompat
import android.text.InputType
import android.util.AttributeSet
import android.view.animation.AccelerateDecelerateInterpolator
import android.widget.LinearLayout
import com.mikepenz.iconics.IconicsDrawable
import dctimx.xbuddy.R
import kotlinx.android.synthetic.main.widget_edit_text.view.*

/**
 * Created by Jose on 21/02/2018.
 */

open class EditText(context: Context, attrs: AttributeSet?) : LinearLayout(context, attrs) {

    init {
        init(attrs)
    }

    private fun init(attrs: AttributeSet?) {

        //Set properties to LinearLayout
        orientation = LinearLayout.VERTICAL

        //Inflate view
        inflate(context, R.layout.widget_edit_text, this)

        //Apply atributes
        val attributes = context.theme.obtainStyledAttributes(attrs, R.styleable.EditText, 0, 0)

        try {

            text.inputType = attributes.getInt(R.styleable.EditText_android_inputType, InputType.TYPE_CLASS_TEXT)
            text.hint = attributes.getString(R.styleable.EditText_et_hint)
            text.typeface = Typeface.DEFAULT
            label.text = attributes.getString(R.styleable.EditText_et_label)

            if (!isInEditMode){

                icon.icon = IconicsDrawable(context)
                        .icon("gmd-${attributes.getString(R.styleable.EditText_et_icon)}")
                        .colorRes(R.color.colorIcon)
                        .paddingDp(5)
                        .roundedCornersDp(22)
                        .backgroundColorRes(android.R.color.transparent)

            }



        } finally {
            attributes.recycle()
        }

        //

        text.setOnFocusChangeListener { view, b ->

            if(b){
                label.setTypeface(text.typeface, Typeface.BOLD)
                animateColorIcon(colorFrom = R.color.colorIcon, colorTo = R.color.colorWhite)
                animateBackgroundIcon(colorFrom = android.R.color.transparent, colorTo = R.color.colorAccent)
                animateColorLine(colorFrom = R.color.colorGrayLight, colorTo = R.color.colorAccent)
                animateColorLabel(colorFrom = R.color.colorText, colorTo = R.color.colorAccent)

            } else {
                label.setTypeface(text.typeface, Typeface.NORMAL)
                animateColorIcon(colorFrom = R.color.colorWhite, colorTo = R.color.colorIcon)
                animateBackgroundIcon(colorFrom = R.color.colorAccent, colorTo = android.R.color.transparent)
                animateColorLine(colorFrom = R.color.colorAccent, colorTo = R.color.colorGrayLight)
                animateColorLabel(colorFrom = R.color.colorAccent, colorTo = R.color.colorText)

            }
        }

    }

    private fun animateColorIcon(colorFrom: Int, colorTo: Int) {
        val animator = ValueAnimator.ofObject(
                ArgbEvaluator(),
                ContextCompat.getColor(context, colorFrom),
                ContextCompat.getColor(context, colorTo)
        )
        animator.duration = 250
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener { icon.setColorFilter(it.animatedValue as Int) }
        animator.start()
    }

    private fun animateBackgroundIcon(colorFrom: Int, colorTo: Int) {
        val animator = ValueAnimator.ofObject(
                ArgbEvaluator(),
                ContextCompat.getColor(context, colorFrom),
                ContextCompat.getColor(context, colorTo)
        )
        animator.duration = 250
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener { icon.icon.backgroundColor(it.animatedValue as Int) }
        animator.start()
    }

    private fun animateColorLine(colorFrom: Int, colorTo: Int) {
        val animator = ValueAnimator.ofObject(
                ArgbEvaluator(),
                ContextCompat.getColor(context, colorFrom),
                ContextCompat.getColor(context, colorTo)
        )
        animator.duration = 250
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener { line.setBackgroundColor(it.animatedValue as Int) }
        animator.start()
    }

    private fun animateColorLabel(colorFrom: Int, colorTo: Int) {
        val animator = ValueAnimator.ofObject(
                ArgbEvaluator(),
                ContextCompat.getColor(context, colorFrom),
                ContextCompat.getColor(context, colorTo)
        )
        animator.duration = 250
        animator.interpolator = AccelerateDecelerateInterpolator()
        animator.addUpdateListener { label.setTextColor(it.animatedValue as Int) }
        animator.start()
    }

    fun getText() = text.text

    fun getEditText() = text

}
