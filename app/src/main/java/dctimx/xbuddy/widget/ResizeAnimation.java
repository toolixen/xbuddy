package dctimx.xbuddy.widget;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Jose on 23/02/2018.
 */

public class ResizeAnimation extends Animation {

	final int targetHeight;
	View view;
	int startHeight;

	public ResizeAnimation(int targetHeight, View view, int startHeight) {
		this.targetHeight = targetHeight;
		this.view = view;
		this.startHeight = startHeight;
	}

	@Override
	protected void applyTransformation(float interpolatedTime, Transformation t) {
		view.getLayoutParams().height = (int) (startHeight + (targetHeight - startHeight) * interpolatedTime);
		view.requestLayout();
	}

	@Override
	public void initialize(int width, int height, int parentWidth, int parentHeight) {
		super.initialize(width, height, parentWidth, parentHeight);
	}

	@Override
	public boolean willChangeBounds() {
		return true;
	}
}
